package util;

import java.io.*;
import java.util.ArrayList;

public class Utility {
	
	public static BufferedReader justGiveMeABufferReader(String filename){
        String filePath = new File("").getAbsolutePath();
        String completePath = filePath + "\\input\\" + filename;
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(completePath));
        } catch (FileNotFoundException ex) {
            System.out.println("FILE NOT FOUND!");
        } 
        return in;
    }

    // better to change every line into a String[] according to the specs
    public static String read(String filename) {
        String filePath = new File("").getAbsolutePath();
        String completePath = filePath + "\\input\\" + filename;
        String output = "";
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(completePath));
            String line = in.readLine();
            int i = 0;
            while (line != null) {
                output += line + " ";
                line = in.readLine();
            }
            System.out.println("Finished Reading File: " + filename);
        } catch (FileNotFoundException fnte) {
            System.out.println("File Not Found");
            fnte.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Input Output Exception");
            ioe.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ioe) {
                    System.out.println("Input Output Exception while Closing BufferedReader");
                    ioe.printStackTrace();
                }
            }
        }
        return output;
    }
    
        public static void write(String filename, String data){
        String filePath = new File("").getAbsolutePath();
        System.out.println(filePath);
        String completePath = filePath + "\\output\\" + filename;
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(completePath, true));
            writer.write(data);
            System.out.println("Written file " + filename + " in output folder.");
        } catch (IOException ioe){
            System.out.println("IOEx while writing");
            ioe.printStackTrace();
        } finally {
            try{
                if(writer != null){
                    writer.close();
                }
            } catch (IOException ioe){
                System.out.println("IOEx while closing writer");
                ioe.printStackTrace();
            }
        }
    }
//        public static void fromSolutionToFile(ArrayList<Vehicle> solution, String filename) {
//        	String output = "";
//            for(Vehicle route : solution) {
//            	output += route.toString() + "\n";
//            }
//            Utility.write(filename + "_solution", output);
//        }

}
