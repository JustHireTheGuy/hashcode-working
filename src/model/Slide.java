package model;

import java.util.HashSet;

public class Slide {
	
	final Photo photo1;
	final Photo photo2;
	final HashSet<String> tags;
	
	
	public Slide(Photo photo1, Photo photo2) {
		this.photo1 = photo1;
		this.photo2 = photo2;
		this.tags = new HashSet<>(photo1.getTags());
		tags.addAll(photo2.getTags());
	}
	
	public Slide(Photo photo1) {
		this.photo1 = photo1;
		this.tags = new HashSet<>(photo1.getTags());
		this.photo2 = null;
	}

	public Photo getPhoto1() {
		return photo1;
	}

	public Photo getPhoto2() {
		return photo2;
	}

	public HashSet<String> getTags() {
		return tags;
	}

	@Override
	public String toString() {
		String output = "" + photo1.getId();
		if (photo2 != null) {
			output += " " + photo2.getId();
		}
		return output;
	}
	
	
	
	
}
