package model;

import java.util.ArrayList;
import java.util.HashSet;

public class HeuristicPlacer {

	public static int scoreSlideCombination(Slide slide1, Slide slide2) {
		HashSet<String> commonTags = new HashSet<String>(slide1.getTags());
		commonTags.retainAll(slide2.getTags());
		int commonTagsNum = commonTags.size();
		int uniqueTagsSlide1 = slide1.getTags().size() - commonTagsNum;
		int uniqueTagsSlide2 = slide2.getTags().size() - commonTagsNum;
		int max = Math.max(commonTagsNum, uniqueTagsSlide1);
		max = Math.max(max, uniqueTagsSlide2);
		return max;
	}

	public static ArrayList<Slide> heuristicPlacer(Problem problem) {
		System.out.println("A");
		ArrayList<Photo> photos = problem.getPhotos();
		int numOfPhotos = problem.getNumOfPhotos();
		ArrayList<Slide> generatedSlides = new ArrayList<>();
		boolean orizontalDone = false;
		while (!orizontalDone) {
			int pos = -1;
			for (int i = 0; i < photos.size(); i++) {
				Photo photo = photos.get(i);
				if (!photo.isVertical) {
					pos = i;
					break;
				}
			}
			if (pos != -1) {
				Slide slide = new Slide(photos.get(pos));
				generatedSlides.add(slide);
				photos.remove(pos);
			} else {
				orizontalDone = true;
			}
		}

		boolean done = false;
		while (!done) {
			done = true;
			int bestComboVertical1 = -1;
			int bestComboVertical2 = -1;
			int commonTag = -1;
			for (int i = 0; i < photos.size() - 1; i++) {
				Photo currentPhoto = photos.get(i);
				if (currentPhoto.isVertical) {
					for (int j = i + 1; j < photos.size(); j++) {
						Photo currentPhoto2 = photos.get(j);
						if (currentPhoto2.isVertical) {
							HashSet<String> commonTags = new HashSet<String>(currentPhoto.getTags());
							commonTags.retainAll(currentPhoto2.getTags());
							if (commonTag == -1) {
								bestComboVertical1 = i;
								bestComboVertical2 = j;
								commonTag = commonTags.size();
								done = false;
							} else {
								if (commonTags.size() < commonTag) {
									commonTag = commonTags.size();
									bestComboVertical1 = i;
									bestComboVertical2 = j;
									done = false;
								}
							}
						}
					}
				}
			}

			if (!done) {
				Photo vertical1 = photos.get(bestComboVertical1);
				Photo vertical2 = photos.get(bestComboVertical2);
				photos.remove(vertical1);
				photos.remove(vertical2);
				Slide slide = new Slide(vertical1, vertical2);
				generatedSlides.add(slide);
			}
		}

		System.out.println("Generated Slides: " + generatedSlides.size());

		// SOMEHOW TO SORT HERE

		Slide fattestSlide = generatedSlides.get(0);
		int tagNumber = fattestSlide.getTags().size();
		for (int i = 1; i < generatedSlides.size(); i++) {
			Slide currentSlide = generatedSlides.get(i);
			int currentTagNum = currentSlide.getTags().size();
			if (currentTagNum > tagNumber) {
				tagNumber = currentTagNum;
				fattestSlide = currentSlide;
			}
		}

		
		System.out.println("before headTailChain()");
		ArrayList<Slide> output = headTailChain(fattestSlide, generatedSlides);
		return output;
	}

	public static Slide bestMatch(Slide slide, ArrayList<Slide> slides) {
		int bestScore = 0;
		Slide output = null;
		for (int i = 0; i < slides.size(); i++) {
			int score = scoreSlideCombination(slide, slides.get(i));
			if (score > bestScore) {
				bestScore = score;
				output = slides.get(i);
			}
		}
		return output;
	}

	public static ArrayList<Slide> headTailChain(Slide startingPoint, ArrayList<Slide> slides) {
		ArrayList<Slide> editableSlides = new ArrayList<Slide>(slides);
		ArrayList<Slide> solution = new ArrayList<Slide>();

		solution.add(startingPoint);
		editableSlides.remove(startingPoint);

		boolean done = false;
		System.out.println("in headTailChain before while");
		while (!done) {
			System.out.println(editableSlides.size());
			done = true;
			Slide head = solution.get(0);
			Slide tail = solution.get(solution.size() - 1);
			Slide substitution = null;
			Slide bestMatchHead = bestMatch(head, editableSlides);
			Slide bestMatchTail = bestMatch(tail, editableSlides);

			int bestScore = -1;
			if (bestMatchHead != null) {
				bestScore = scoreSlideCombination(head, bestMatchHead);
				substitution = bestMatchHead;
				solution.add(substitution);
				editableSlides.remove(substitution);
				done = false;
			}
			if (bestMatchTail != null) {
				int score = scoreSlideCombination(tail, bestMatchTail);
				if (bestScore == -1 || score > bestScore) {
					bestScore = score;
					substitution = bestMatchTail;
					solution.add(substitution);
					editableSlides.remove(substitution);
					done = false;
				}
			}
		}
		return solution;
	}
}
