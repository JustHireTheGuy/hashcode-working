package model;

import java.util.ArrayList;

public class Problem {
	
	// Problem(gridRows, gridColumns, numVehicles, numRides,bonusStartOnTime,totalSteps);
    final int numOfPhotos;
    final ArrayList<Photo> photos;
    
    public Problem(int numOfPhotos, ArrayList<Photo> photos) {
    	this.numOfPhotos = numOfPhotos;
    	this.photos = photos;
    }

	public int getNumOfPhotos() {
		return numOfPhotos;
	}

	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	@Override
	public String toString() {
		String output = ""+ numOfPhotos + "\n";
		for (Photo photo : photos) {
			output += photo.toString();
		}
		return output;
	}
    
    
    
}
