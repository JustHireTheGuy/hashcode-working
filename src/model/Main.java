package model;

import java.io.BufferedReader;
import java.util.ArrayList;

import util.Utility;


public class Main {
	public static void main(String[] args) throws CloneNotSupportedException{
        String filename = "b_lovely_landscapes.txt";
        BufferedReader reader = Utility.justGiveMeABufferReader(filename);
        Problem problem = DecodeProblem.DecodeInput(reader);
        
        ArrayList<Slide> solution = HeuristicPlacer.heuristicPlacer(problem);
        String output = "" + solution.size() + "\n";
        for(Slide slide : solution) {
        	output += "" + slide.toString() + "\n";
        }
       
        
        System.out.println("***EVERYTHING FINISHED***");
        Utility.write(filename + "_solution", output);
        System.out.println("output");
        
    }

}
