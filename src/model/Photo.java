package model;

import java.util.HashSet;

public class Photo {
	
	final int id;
	final boolean isVertical;
	final int numOfTags;
	final HashSet<String> tags;
	
	public Photo(int id, boolean isVertical, int numOfTags, HashSet<String> tags) {
		this.id = id;
		this.isVertical = isVertical;
		this.numOfTags = numOfTags;
		this.tags = tags;
	}
	public HashSet<String> getTags() {
		return tags;
	}

	public boolean isVertical() {
		return isVertical;
	}
	public int getNumOfTags() {
		return numOfTags;
	}
	
	@Override
	public String toString() {
		return "Vertical? : " + isVertical + " - tagNum: " + numOfTags + " - tags " + tags.toString() + "\n";
	}
	public int getId() {
		return id;
	}
	
	
}
