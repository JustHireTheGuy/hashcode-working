package model;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class DecodeProblem {


    public static Problem DecodeInput(BufferedReader in) {

        Problem problem = null;

        if (in != null) {
            try {
                //first line breakdown
                String[] firstLn = in.readLine().split(" ");
                int numOfPhotos = Integer.parseInt(firstLn[0]);


                // for each line after the first create a Ride
                ArrayList<Photo> photos = new ArrayList<>();
                for(int i = 0; i < numOfPhotos; i++){
                    String[] line = in.readLine().split(" ");
                    boolean isVertical = line[0].equals("V");
                    int tagNum = Integer.parseInt(line[1]);
                    HashSet<String> tags = new HashSet<>();
                    for(int j = 2; j < tagNum + 2; j++) {
                    	tags.add(line[j]);
                    }
                    
                    Photo photo = new Photo(i, isVertical,tagNum,tags);
                    photos.add(photo);
         
                }
                problem = new Problem(numOfPhotos, photos);


            } catch (FileNotFoundException fnte) {
                System.out.println("File Not Found");
                fnte.printStackTrace();
            } catch (IOException ioe) {
                System.out.println("Input Output Exception");
                ioe.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ioe) {
                        System.out.println("Input Output Exception while Closing BufferedReader");
                        ioe.printStackTrace();
                    }
                }
            }
            System.out.println("End of Problem Loading");
            return problem;
        } else {
        	System.out.println("BufferReader is null!!!");
        	return null;
        }
    }
}
